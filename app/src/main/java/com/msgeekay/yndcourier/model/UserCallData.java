package com.msgeekay.yndcourier.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class UserCallData extends BaseEntitySynchronizer
{
  private long userCallDataId;
  private long userCallDataUserId;
  private long userCallDataTimestamp;
  private String userCallDataPhone;
  private long userCallDataDuration;

}
