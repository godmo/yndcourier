package com.msgeekay.yndcourier.model;

import android.os.SystemClock;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class Order extends BaseEntitySynchronizer
{
  private long orderId;
  private long orderRemoteId;
  private long orderUserId;
  private long orderTimestamp;
  private long orderState;

  private long orderTimeRangeId;
  private long orderTimeRangeStartId;
  private long orderTimeRangeEndId;
  private long orderTimeRangeStart;
  private long orderTimeRangeEnd;
  private long orderCustomTime;

  private String orderCustomerName;
  private long orderCustomerPhone;
  private long orderSum;
  private long orderISOCurrencyId;
  private long orderPaymentTypeId;

  private String orderRemoteComment;
  private String orderComment;

  private double orderLocationLongitude;
  private double orderLocationLatitude;
  private String orderLocationName;

  public Order(long orderRemoteId, long orderUserId, long orderTimeRangeId, String orderCustomerName, long orderCustomerPhone,
               long orderSum, long orderISOCurrencyId, long orderPaymentTypeId, String orderRemoteComment, double lat, double lon,
               String orderLocationName)
  {
    this.orderRemoteId = orderRemoteId;
    this.orderUserId = orderUserId;
    this.orderTimeRangeId = orderTimeRangeId;
    this.orderCustomerName = orderCustomerName;
    this.orderCustomerPhone = orderCustomerPhone;
    this.orderSum = orderSum;
    this.orderISOCurrencyId = orderISOCurrencyId;
    this.orderPaymentTypeId = orderPaymentTypeId;
    this.orderRemoteComment = orderRemoteComment;
    this.orderLocationLatitude = lat;
    this.orderLocationLongitude = lon;
    this.orderLocationName = orderLocationName;

    this.orderState = OrderState.OrderStateEnum.orderNew.ordinal();
    this.orderTimestamp = SystemClock.currentThreadTimeMillis();
    this.setEntitySynchronizationState(SynchronizationState.stateOk.ordinal());
    this.setEntitySynchronizationTimestamp(SystemClock.currentThreadTimeMillis());
  }


  public long getOrderState()
  {
    return orderState;
  }

  public void setOrderState(long orderState)
  {
    this.orderState = orderState;
    this.setEntitySynchronizationState(SynchronizationState.stateNeedsSync.ordinal());
  }

  public long getOrderCustomTime()
  {
    return orderCustomTime;
  }

  public void setOrderCustomTime(long orderCustomTime)
  {
    this.orderCustomTime = orderCustomTime;
    this.setEntitySynchronizationState(SynchronizationState.stateNeedsSync.ordinal());
  }

  public long getOrderTimeRangeId()
  {
    return orderTimeRangeId;
  }

  public void setOrderTimeRangeId(long orderTimeRangeId)
  {
    this.orderTimeRangeId = orderTimeRangeId;
    this.setEntitySynchronizationState(SynchronizationState.stateNeedsSync.ordinal());
  }

  public String getOrderCustomerName()
  {
    return orderCustomerName;
  }

  public long getOrderCustomerPhone()
  {
    return orderCustomerPhone;
  }

  public long getOrderSum()
  {
    return orderSum;
  }

  public long getOrderPaymentTypeId()
  {
    return orderPaymentTypeId;
  }

  public void setOrderPaymentTypeId(long orderPaymentTypeId)
  {
    this.orderPaymentTypeId = orderPaymentTypeId;
    this.setEntitySynchronizationState(SynchronizationState.stateNeedsSync.ordinal());
  }

  public String getOrderRemoteComment()
  {
    return orderRemoteComment;
  }

  public void setOrderComment(String orderComment)
  {
    this.orderComment = orderComment;
    this.setEntitySynchronizationState(SynchronizationState.stateNeedsSync.ordinal());
  }

  public double getOrderLocationLongitude()
  {
    return orderLocationLongitude;
  }
  public void setOrderLocationLongitude(double orderLocationLongitude)
  {
    this.orderLocationLongitude = orderLocationLongitude;
  }

  public double getOrderLocationLatitude()
  {
    return orderLocationLatitude;
  }
  public void setOrderLocationLatitude(double orderLocationLatitude)
  {
    this.orderLocationLatitude = orderLocationLatitude;
  }

  public String getOrderLocationName()
  {
    return orderLocationName;
  }
}
