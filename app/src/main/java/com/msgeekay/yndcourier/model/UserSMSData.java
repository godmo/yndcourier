package com.msgeekay.yndcourier.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class UserSMSData extends BaseEntitySynchronizer
{
  private long userSMSDataId;
  private long userSMSDataUserId;
  private long userSMSDataTimestamp;
  private String userSMSDataMessage;
}
