package com.msgeekay.yndcourier.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class OrderState
{
  public enum OrderStateEnum {
    orderNew,
    orderApproved,
    orderRescheduled,
    orderDelivered,
    orderCancelled
  }

  private long orderStateId;
  private String orderStateName;
}
