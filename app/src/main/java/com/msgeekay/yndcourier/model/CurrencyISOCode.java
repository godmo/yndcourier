package com.msgeekay.yndcourier.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class CurrencyISOCode
{
  public enum ISOCode {
    RUB,
    USD,
    EUR
  }

  private long currencyId;
  private String currencyISOCode;
}
