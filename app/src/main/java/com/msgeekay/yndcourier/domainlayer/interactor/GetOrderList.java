package com.msgeekay.yndcourier.domainlayer.interactor;

import android.hardware.Camera;

import com.msgeekay.yndcourier.datalayer.net.Api;
import com.msgeekay.yndcourier.domainlayer.executor.PostExecutionThread;
import com.msgeekay.yndcourier.domainlayer.executor.ThreadExecutor;
import com.msgeekay.yndcourier.model.Order;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 16/05/17.
 */

public class GetOrderList extends UseCase
{
  private Api api;

  @Inject
  public GetOrderList(Api a, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.api = a;
  }

  @Override
  protected Observable<List<Order>> buildUseCaseObservable()
  {
    if (api != null)
      return api.orders();
    else
      return api.emptyOrders();
  }
}
