package com.msgeekay.yndcourier.infrastructurelayer;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public interface CheckStateCallback
{
  public void onCheckStateCallback();
}
