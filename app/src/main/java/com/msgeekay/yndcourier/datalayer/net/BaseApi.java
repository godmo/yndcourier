package com.msgeekay.yndcourier.datalayer.net;

import com.msgeekay.yndcourier.model.Order;

import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 14/05/17.
 */

public interface BaseApi
{
  public Observable<List<Order>> orders();
  public void method();
  public List<Order> dummySetupRV();

  public Observable<List<Order>> emptyOrders();

}
