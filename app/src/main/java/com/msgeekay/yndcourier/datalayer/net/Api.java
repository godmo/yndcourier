package com.msgeekay.yndcourier.datalayer.net;

import android.content.Context;

import com.google.gson.JsonSyntaxException;
import com.msgeekay.yndcourier.datalayer.exception.NetworkConnectionException;
import com.msgeekay.yndcourier.infrastructurelayer.receivers.ConnectionChangeReceiver;
import com.msgeekay.yndcourier.model.Order;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 16/05/17.
 */
@Singleton
public class Api implements BaseApi
{
  //@Inject
  Context context;

  public Api(Context context)
  {
    this.context = context;
  }

  public Observable<List<Order>> orders()
  {
    final List<Order> responseOrders = new ArrayList<>();
    return Observable.create(new Observable.OnSubscribe<List<Order>>()
    {
      @Override
      public void call(Subscriber<? super List<Order>> subscriber)
      {
        if (ConnectionChangeReceiver.isInternetConnectedSilent(context))
        {
          try
          {
            responseOrders.addAll(dummySetupRV());

            if (responseOrders != null)
            {
              subscriber.onNext(responseOrders);
              subscriber.onCompleted();

              //FIXME: implement real-world api
              //String responseOrders = getOrdersFromApi();
              //responseOrders = loadJSONFromAsset();
              //subscriber.onNext(dummySetupRV());

              //subscriber.onNext(ordersEntityJsonMapper.transformOrdersEntity(responseOrders));

            }
            else
            {
              subscriber.onError(new NetworkConnectionException());
            }

          }
          catch (Exception e)
          {
            subscriber.onError(new NetworkConnectionException(e.getCause()));
          }
        }
        else
        {
          subscriber.onError(new NetworkConnectionException());
        }
      }
    });
  }

//  public Observable<List<Order>> orders1()
//  {
//    final List<Order> responseOrders = new ArrayList<>();
//    return Observable.create(subscriber ->
//    {
//      if (ConnectionChangeReceiver.isInternetConnectedSilent(context))
//      {
//        try
//        {
//          responseOrders.addAll(dummySetupRV());
//
//          if (responseOrders != null)
//          {
//            subscriber.onNext(responseOrders);
//            subscriber.onCompleted();
//
//            //FIXME: implement real-world api
//            //String responseOrders = getOrdersFromApi();
//            //responseOrders = loadJSONFromAsset();
//            //subscriber.onNext(dummySetupRV());
//
//            //subscriber.onNext(ordersEntityJsonMapper.transformOrdersEntity(responseOrders));
//
//          }
//          else
//          {
//            subscriber.onError(new NetworkConnectionException());
//          }
//        }
//        catch (Exception e)
//        {
//          subscriber.onError(new NetworkConnectionException(e.getCause()));
//        }
//      }
//      else
//      {
//        subscriber.onError(new NetworkConnectionException());
//      }
//    });
//  }

  public void method()
  {

  };

  public Observable<List<Order>> emptyOrders()
  {
    final List<Order> responseOrders = new ArrayList<>();
    return Observable.create(new Observable.OnSubscribe<List<Order>>()
    {
      @Override
      public void call(Subscriber<? super List<Order>> subscriber)
      {
        subscriber.onNext(responseOrders);
        subscriber.onCompleted();
        //subscriber.onError(new NetworkConnectionException());
      }
    });
//    return Observable.create(subscriber ->
//    {
//        subscriber.onNext(responseOrders);
//        subscriber.onCompleted();
//        //subscriber.onError(new NetworkConnectionException());
//    });
  }

  public List<Order> dummySetupRV() throws JsonSyntaxException
  {
    try
    {
      double[][] coords = new double[][]{
              {55.771052, 37.593438},
              {55.772718, 37.595026},
              {55.773514, 37.592579},
              {55.773032, 37.588460},
              {55.765451, 37.599918},
              {55.771535, 37.603609},
              {55.775059, 37.585413},
              {55.767528, 37.586872},
              {55.775276, 37.597085},
              {55.766996, 37.595841},
              {55.763761, 37.590562},
              {55.765596, 37.599446}
      };

      ArrayList<Order> ordersCollection = new ArrayList<>();
      Order or = new Order(1, 1, 6, "Customer1", 65691, 500000, 1, 1, "Podyezd 2", 55.771052, 37.593438, "Moscow");
      for (int i = 0; i < 12; i++)
      {
        or.setOrderLocationLatitude(coords[i][0]);
        or.setOrderLocationLongitude(coords[i][1]);
        ordersCollection.add(or);
      }

      if (ordersCollection != null)
        return ordersCollection;
      else
        return new ArrayList<>();
    }
    catch (JsonSyntaxException jsonException)
    {
      throw jsonException;
    }
  }
}
