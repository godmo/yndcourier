package com.msgeekay.yndcourier.presentationlayer.ui.main;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.msgeekay.yndcourier.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.yndcourier.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.yndcourier.domainlayer.interactor.UseCase;
import com.msgeekay.yndcourier.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.yndcourier.model.Order;
import com.msgeekay.yndcourier.presentationlayer.MyApp;
import com.msgeekay.yndcourier.presentationlayer.exception.ErrorMessageFactory;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

import static com.msgeekay.yndcourier.presentationlayer.ui.main.MainPresenter.TrackingState.Disabled;
import static com.msgeekay.yndcourier.presentationlayer.ui.main.MainPresenter.TrackingState.Enabled;

/**
 * Created by grigoriykatz on 09/05/17.
 */
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView>
{

  public class TrackingState
  {
    public static final int Enabled = 0;
    public static final int Disabled = 1;
  }

  //@Inject @Named("orderList") UseCase getOrderListUseCase;
  @Inject Context context;
  @Inject ServiceCommunicationManager serviceCommunicationManager;

    UseCase getOrderListUseCase;

  private int trackingState = Disabled;

  //public MainPresenter() {}

  @Inject
  public MainPresenter(@Named("orderList") UseCase getOrderListUseCase)
  {
    Timber.e(getClass().getName(), "onCreateStart before injection");
    MyApp.get().getApplicationComponent().inject(this);
    this.getOrderListUseCase = getOrderListUseCase;
//    this.context = context;
//    this.serviceCommunicationManager = serviceCommunicationManager;

//    retainBlockComponent = DaggerRetainBlockComponent.builder()
//            .applicationComponent(MyApp.getApplicationComponent()).build();
//            //.retainBlockModule(new RetainBlockModule()).build();



    //retainBlockComponent.inject(this);
    Timber.e(getClass().getName(), "onCreateStart after injection");
    getOrderList();
  }

  public void switchTracking()
  {
    if (trackingState == Disabled)
    {
      serviceCommunicationManager.startTracking();
      trackingState = Enabled;
    }
    else
    {
      serviceCommunicationManager.stopTracking();
      trackingState = Disabled;
    }
  }

  private void hideViewLoading() {
    getViewState().hideViewLoading();
  }

  public void showErrorInView(String errorMessage)
  {
    getViewState().showErrorMessage(errorMessage);
    getViewState().showViewRetry();
  }

  public void showOrderCollectionInView(List<Order> orders)
  {
    getViewState().renderOrdersList(orders);
  }

  public void getOrderList()
  {
    Timber.e(getClass().getName(), "getOrderList start");
    this.getOrderListUseCase.execute(new OrderSubscriber());
  }

  @RxLogSubscriber
  private final class OrderSubscriber extends DefaultSubscriber<List<Order>>
  {
    public OrderSubscriber() {}

    @Override public void onCompleted() {
      MainPresenter.this.hideViewLoading();
    }

    @Override public void onError(Throwable e) {

      String errorMessage = ErrorMessageFactory.create(context, new DefaultErrorBundle((Exception) e).getException());
      MainPresenter.this.showErrorInView(errorMessage);

    }

    @Override public void onNext(List<Order> orders) {
      MainPresenter.this.showOrderCollectionInView(orders);
    }
  }
}
