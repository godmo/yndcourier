package com.msgeekay.yndcourier.presentationlayer.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.msgeekay.yndcourier.R;
import com.msgeekay.yndcourier.model.Order;
import com.msgeekay.yndcourier.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.yndcourier.presentationlayer.ui.common.mvp.BaseMvpActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.yandex.yandexmapkit.MapView;
import timber.log.Timber;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class MainActivity extends BaseMvpActivity implements MainView
{
  @BindView(R.id.main_map)
  MapView map;

  @BindView(R.id.main_orders_rv)
  RecyclerView rv_orders;

  RecyclerAdapter recyclerAdapter;

  @InjectPresenter(type = PresenterType.GLOBAL)
  MainPresenter mainPresenter;

  @ProvidePresenter(type = PresenterType.GLOBAL)
  MainPresenter provideMainPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideMainPresenter();
  }

  private Handler mHandler = new Handler();

  public static Intent getCallingIntent(Context context)
  {
    Intent callingIntent = new Intent(context, MainActivity.class);
    return callingIntent;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Timberlg(getClass().getName(), "onCreate");
    setTheme(R.style.AppTheme_MainActivity);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState)
  {
    super.onPostCreate(savedInstanceState);
    setupRecyclerView();
  }

  @OnClick({R.id.main_fab})
  public void onButtonClick(View view)
  {
    mainPresenter.switchTracking();

  }

  public void renderOrdersList(final List<Order> ordersCollection)
  {
    mHandler.postDelayed(new Runnable()
    {
      @Override
      public void run()
      {
        if (ordersCollection != null) {
          MainActivity.this.recyclerAdapter.setOrdersCollection(ordersCollection);
        }
      }
    }, 600);

  }

  private void setupRecyclerView() {
    this.recyclerAdapter = new RecyclerAdapter(this);
    //this.recyclerAdapter.setOnItemClickListener(onItemClickListener);
    this.rv_orders.setLayoutManager(new OrdersLayoutManager(this));
    this.rv_orders.setAdapter(recyclerAdapter);
    this.rv_orders.setNestedScrollingEnabled(false);
    //this.rv_orders.addOnScrollListener(onScrollListener);
    //this.rv_news.setItemAnimator(new RecyclerItemAnimator());
    //rv_news.s
  }



  @Override
  public void hideViewLoading()
  {

  }

  @Override
  public void showErrorMessage(String errorMessage)
  {

  }

  @Override
  public void showViewRetry()
  {

  }
}
