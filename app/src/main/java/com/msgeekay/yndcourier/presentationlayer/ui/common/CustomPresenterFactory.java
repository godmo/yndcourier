package com.msgeekay.yndcourier.presentationlayer.ui.common;

import com.arellomobile.mvp.PresenterStore;
import com.msgeekay.yndcourier.presentationlayer.MyApp;
import com.msgeekay.yndcourier.presentationlayer.ui.main.MainPresenter;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/05/17.
 */

public class CustomPresenterFactory //implements PresenterFactory //extends PresenterStore
{
  @Inject
  MainPresenter mainPresenter;

  public CustomPresenterFactory()
  {
    MyApp.get().getApplicationComponent().inject(this);
  }

  public MainPresenter provideMainPresenter()
  {
    return mainPresenter;
  }

}