package com.msgeekay.yndcourier.presentationlayer.ui.main;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by grigoriykatz on 11/05/17.
 *
 * Layout manager to position items inside a {@link android.support.v7.widget.RecyclerView}.
 */
public class OrdersLayoutManager extends LinearLayoutManager
{

  private static final float MILLISECONDS_PER_INCH = 50f;
  private Context mContext;

  private Runnable endOfScrollToPositionRunnable = null;

  public OrdersLayoutManager(Context context)
  {
    super(context);
    mContext = context;
  }
}