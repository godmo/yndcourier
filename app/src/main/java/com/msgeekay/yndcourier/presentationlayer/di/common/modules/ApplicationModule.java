package com.msgeekay.yndcourier.presentationlayer.di.common.modules;


import android.content.Context;

import com.msgeekay.yndcourier.datalayer.executor.JobExecutor;
import com.msgeekay.yndcourier.datalayer.net.Api;
import com.msgeekay.yndcourier.domainlayer.executor.PostExecutionThread;
import com.msgeekay.yndcourier.domainlayer.executor.ThreadExecutor;
import com.msgeekay.yndcourier.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.yndcourier.presentationlayer.MyApp;
import com.msgeekay.yndcourier.presentationlayer.Preferences;
import com.msgeekay.yndcourier.presentationlayer.UIThread;

import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
  private final MyApp application;
  private final Preferences prefs;
  private final ServiceCommunicationManager serviceCommunicationManager;

  public ApplicationModule(MyApp application) {
    this.application = application;
    this.prefs = new Preferences(application);
    this.serviceCommunicationManager = new ServiceCommunicationManager(application.getBaseContext());
    this.serviceCommunicationManager.bindService();
  }

  @Provides
  @Singleton
  Context provideApplicationContext() {
    return this.application.getApplicationContext();
  }

  @Provides @Singleton
  ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
    return jobExecutor;
  }

  @Provides @Singleton
  PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides
  @Singleton
  Preferences providePreferences() {
    return prefs;
  }

  @Provides
  @Singleton
  ServiceCommunicationManager providesServiceCommunicationManager() {
    return serviceCommunicationManager;
  }

  @Provides
  @Singleton
  Api providesApi() {
    return new Api(application.getApplicationContext());
  }

  @Provides
  @Singleton
  @Named("ProcName")
  String providesAppProcessName()
  {
    return application.getAppProcessName();
  }

//  @Provides @Singleton UserCache provideUserCache(UserCacheImpl userCache) {
//    return userCache;
//  }
//
//  @Provides @Singleton
//  NewsFeedCache provideNewsFeedCache(NewsFeedCacheImpl newsCache) {
//    return newsCache;
//  }

}

