package com.msgeekay.yndcourier.presentationlayer.di.common.modules;

import com.msgeekay.yndcourier.datalayer.net.Api;
import com.msgeekay.yndcourier.domainlayer.executor.PostExecutionThread;
import com.msgeekay.yndcourier.domainlayer.executor.ThreadExecutor;
import com.msgeekay.yndcourier.domainlayer.interactor.GetOrderList;
import com.msgeekay.yndcourier.domainlayer.interactor.UseCase;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grigoriykatz on 18/05/17.
 */

@Module
public class UseCasesModule
{
  @Provides
  @Singleton
  @Named("orderList")
  UseCase provideGetOrderListUseCase(Api o, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    return new GetOrderList(o, threadExecutor, postExecutionThread);
  }
}
