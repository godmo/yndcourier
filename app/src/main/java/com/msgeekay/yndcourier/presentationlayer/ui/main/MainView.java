package com.msgeekay.yndcourier.presentationlayer.ui.main;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.yndcourier.domainlayer.exception.ErrorBundle;
import com.msgeekay.yndcourier.model.Order;
import com.msgeekay.yndcourier.presentationlayer.exception.ErrorMessageFactory;

import java.util.List;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public interface MainView extends MvpView
{
  public void hideViewLoading();

  public void showErrorMessage(String errorMessage);

  public void showViewRetry();

  public void renderOrdersList(List<Order> orders);
}
