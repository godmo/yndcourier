package com.msgeekay.yndcourier.presentationlayer.di.common.components;

import android.content.Context;

import com.msgeekay.yndcourier.datalayer.net.Api;
import com.msgeekay.yndcourier.domainlayer.executor.PostExecutionThread;
import com.msgeekay.yndcourier.domainlayer.executor.ThreadExecutor;
import com.msgeekay.yndcourier.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.yndcourier.presentationlayer.Preferences;
import com.msgeekay.yndcourier.presentationlayer.di.common.modules.ApplicationModule;
import com.msgeekay.yndcourier.presentationlayer.di.common.modules.UseCasesModule;
import com.msgeekay.yndcourier.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.yndcourier.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.yndcourier.presentationlayer.ui.main.MainPresenter;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * A component whose lifetime is the life of the application.
 */

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {ApplicationModule.class, UseCasesModule.class})
public interface ApplicationComponent
{
  void inject(BaseMvpActivity baseActivity);
  void inject(CustomPresenterFactory x);
  void inject(MainPresenter x);

  //Exposed to sub-graphs.
  Context context();
  ThreadExecutor threadExecutor();
  PostExecutionThread postExecutionThread();
  Preferences preferences();
  ServiceCommunicationManager serviceCommunicationManager();
  Api api();
}
