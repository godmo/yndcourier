package com.msgeekay.yndcourier.presentationlayer.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.msgeekay.yndcourier.R;
import com.msgeekay.yndcourier.model.Order;
import com.msgeekay.yndcourier.presentationlayer.utils.CustomUtils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  public interface OnItemClickListener {
    void onNewsItemClicked(int position, Order newsItemModel, int[] startingLocation, View view);
  }

  private static final int ANIMATED_ITEMS_COUNT = 3;

  private int lastAnimatedPosition = -1;
  private int itemsCount = 0;

  private List<Order> mItemList;
  private final LayoutInflater layoutInflater;

  private OnItemClickListener onItemClickListener;
  private Context mContext;

  //@Inject
  public RecyclerAdapter(Context context) {
    this.layoutInflater =
            (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    mItemList = Collections.emptyList();
    mContext = context;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, parent, false);


    //final View view = this.layoutInflater.inflate(R.layout.recycler_item, parent, false);
    return new RecyclerItemViewHolder(view);
  }

  private void runEnterAnimation(View view, int position) {
    if (position >= ANIMATED_ITEMS_COUNT - 1) {
      return;
    }

    if (position > lastAnimatedPosition) {
      lastAnimatedPosition = position;
      view.setTranslationY(CustomUtils.getScreenHeight(mContext));
      view.animate()
              .translationY(0)
              .setInterpolator(new DecelerateInterpolator(3.f))
              .setDuration(700)
              .start();
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
  {
    runEnterAnimation(viewHolder.itemView, position);
    RecyclerItemViewHolder holder = (RecyclerItemViewHolder) viewHolder;

    final Order orderItem = mItemList.get(position);
    holder.textViewTitle.setText(orderItem.getOrderCustomerName());
    holder.itemView.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        if (RecyclerAdapter.this.onItemClickListener != null)
        {
          int[] startingLocation = new int[2];
          v.getLocationOnScreen(startingLocation);
          RecyclerAdapter.this.onItemClickListener.onNewsItemClicked(position, orderItem, startingLocation, v.findViewById(R.id.title));
        }
      }
    });
    int width = mContext.getResources().getDisplayMetrics().widthPixels - CustomUtils.dpToPx(32);

    holder.textViewCategories.setText(orderItem.getOrderLocationName());

    //setDate(holder, newsItem.getItemDate());

  }

//  private void setDate(RecyclerItemViewHolder holder, Date date) {
//
//
//    String dateText = "";
//
//    DateTime dt = new DateTime(date);
//
//    Calendar cal = Calendar.getInstance();
//    //Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
//    DateTime now = new DateTime(cal.getTime());
//    int thisMonth = cal.get(Calendar.MONTH);
//    int thisYear = cal.get(Calendar.YEAR);
//
//    cal.add(Calendar.DATE, -1);
//    DateTime yesterday = new DateTime(cal.getTime());
//
//    cal.add(Calendar.DATE, -1);
//    DateTime dayBeforeYesterday = new DateTime(cal.getTime());
//
//
//
//    if (now.withTimeAtStartOfDay().equals(dt.withTimeAtStartOfDay())) {
//      if (now.toDate().getTime() - dt.toDate().getTime() < 3600000) {
//
//        long diff = now.toDate().getTime() - dt.toDate().getTime();
//        int mins = (int)Math.ceil(diff/60000);
//        dateText = String.format(mContext.getResources().getString(R.string.min_ago),
//                Integer.toString(mins));
//      } else {
//        long diff = now.toDate().getTime() - dt.toDate().getTime();
//        int hours = (int)Math.ceil(diff/3600000);
//        dateText = String.format(mContext.getResources().getString(R.string.more_than_hour_ago),
//                Integer.toString(hours));
//      }
//
//    } else if (yesterday.withTimeAtStartOfDay().equals(dt.withTimeAtStartOfDay())) {
//
//      dateText = mContext.getResources().getString(R.string.yesterday);
//
//    } else if (dayBeforeYesterday.withTimeAtStartOfDay().equals(dt.withTimeAtStartOfDay())) {
//
//      dateText = mContext.getResources().getString(R.string.day_before_yesterday);
//
//    } else {
//      SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//      dateText = sdf.format(date);
//    }
//
//    /*
//    if (dt.year().equals(now.year()) && dt.monthOfYear().equals(now.monthOfYear())) {
//
//      long diff = now.toDate().getTime() - dt.toDate().getTime();
//      int days = (int)Math.ceil(diff/86400000);
//      dateText = String.format( mContext.getResources().getString(days < 5 ? R.string.days_ago2nd : R.string.days_ago),
//              Integer.toString(days));
//
//    } else if (dt.year().equals(now.year())) {
//
//      long diff = now.toDate().getTime() - dt.toDate().getTime();
//      int months = now.getMonthOfYear() - dt.getMonthOfYear();
//
//      dateText = months > 1 ? String.format(mContext.getResources().getString(R.string.more_than_months_ago),
//              Integer.toString(months))
//                          : mContext.getResources().getString(R.string.months_ago);
//
//    } else {
//
//      dateText = mContext.getResources().getString(R.string.more_than_year_ago);
//    }
//    */
//
//    holder.textViewDate.setText(dateText);
//
//  }


  @Override
  public int getItemCount() {
    return mItemList == null ? 0 : mItemList.size();
  }

  @Override public long getItemId(int position) {
    return position;
  }

  public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
    this.onItemClickListener = onItemClickListener;
  }

  public void setOrdersCollection(Collection<Order> orderCollection) {
    this.validateOrdersCollection(orderCollection);
    this.mItemList = (List<Order>) orderCollection;
    this.notifyDataSetChanged();
  }

  private void validateOrdersCollection(Collection<Order> orderCollection) {
    if (orderCollection == null) {
      throw new IllegalArgumentException("The list cannot be null");
    }
  }

  static class RecyclerItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.title)
    TextView textViewTitle;

    @BindView(R.id.date)
    TextView textViewDate;

    @BindView(R.id.cats)
    TextView textViewCategories;

    public RecyclerItemViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

}

