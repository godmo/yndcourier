![Scheme](art/logo_small.png =324x90)

Yandex Courier MVP. Demo showcase
====================


- - -


This MVP was implemented as part of one of my sideprojects. 
Since work was holded by a client, I decided to put code into a repo for demo purposes.

![Scheme](art/yndc_small.png)